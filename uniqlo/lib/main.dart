import 'package:flutter/material.dart';
import 'ui/MemberScreen/app_setting_screen.dart';
import 'ui/MemberScreen/profile_setting_screen.dart';
import 'ui/MemberScreen/order_history.dart';
import 'ui/MemberScreen/purchase_history.dart';
import 'ui/MemberScreen/survey_screen.dart';
import 'ui/MemberScreen/coupons_screen.dart';
import 'ui/cart_screen.dart';
import 'ui/notification_screen.dart';
import 'ui/BottomNavigator/bottom_navigator.dart';
import 'ui/search_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Uniqlo',
      routes: {
        '/': (context) => const BotNavigationBar(),
        '/notification': (context) => NotificationScreen(),
        '/cart': (context) => CartScreen(),
        '/search': (context) => SearchScreen(),
        '/coupons': (context) => CouponsScreen(),
        '/survey': (context) => SurveyScreen(),
        '/purchase-history': (context) => PurchaseHistory(),
        '/order-history': (context) => OrderHistory(),
        '/profile-setting': (context) => SettingScreen(),
        '/app-setting': (context) => AppSetting(),
      },
      initialRoute: '/',
    );
  }
}