import 'package:flutter/material.dart';

class NotificationScreen extends StatelessWidget {
  static const route = '/notification';

  const NotificationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tabs = [
      {"text": "Dành cho bạn"},
      {"text": "Có gì mới"},
    ];
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('thông báo'.toUpperCase()),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      body: Column(
        children: [
          Container(
            width: double.infinity,
            color: Colors.white,
            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 32),
            child: Image.asset(
              'assets/notification-img.png',
              fit: BoxFit.fill,
            ),
          ),
          DefaultTabController(
            length: tabs.length,
            child: Column(
              children: [
                Container(
                  color: Colors.white,
                  child: TabBar(
                    labelColor: Colors.black,
                    labelStyle: TextStyle(
                      fontSize: 16, fontWeight: FontWeight.bold
                    ),
                    unselectedLabelColor: Colors.grey,
                    indicatorColor: Colors.black,
                    indicatorWeight: 2,
                    tabs: tabs
                        .map((tab) => Tab(
                              text: tab['text'],
                            ))
                        .toList(),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height -352,
                  child: TabBarView(
                    children: tabs.map((tab) {
                      return Center(
                       child: Text('Bạn không có bất kì thông báo nào.', style: TextStyle(color: Colors.grey[600]),),
                      );
                    }).toList(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
