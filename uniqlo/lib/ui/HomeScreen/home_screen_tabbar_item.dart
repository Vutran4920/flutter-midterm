import 'package:flutter/material.dart';

class TabBarItem extends StatelessWidget {
  const TabBarItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.77,
            decoration: BoxDecoration(
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage(
                  'assets/women-top-img.png',
                ),
              ),
            ),
          ),
          Container(
            height: 150,
            color: Colors.grey[200],
            padding: EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  padding: EdgeInsets.only(right: 4),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/live-station-img.png'),
                      Text(
                        'Live station'.toUpperCase(), maxLines: 1,
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w900,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Livestream on uniqlo's fanpage & web/app".toUpperCase(), overflow: TextOverflow.ellipsis, maxLines: 1,
                        style:
                            TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        'Khám phá thế giới ut đa màu sắc và ưu đãi đặc biệt'
                            .toUpperCase(), overflow: TextOverflow.ellipsis, maxLines: 1,
                        style: TextStyle(fontSize: 12),
                      ),
                      Text(
                        "19:30 | 19.05.2022", overflow: TextOverflow.ellipsis, maxLines: 1,
                        style: TextStyle(fontSize: 12)
                      ),
                      Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(width: 1.5, color: Colors.black)),
                        child: Text(
                          'Đón xem ngay'.toUpperCase(),
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 50),
          BuildBodyContainer(
            title: 'Hàng mới về',
            image: 'assets/hang-moi-ve-img.png',
            name: 'Quần Linen pha dáng rộng',
            description:
                'Quần dáng dộng thoải mái với chất liệu linen pha rayon mềm mát',
            newPrice: '890.00 VND',
            oldPrice: '784.000 VND',
            discountText:
                'Khuyến Mãi Chỉ Áp Dụng Tại Các Hàng Trực Tuyến, SC Vivo City và Sài Gòn Centre',
            discountTime: 'Khuyến Mãi Có Hạn Từ 13.6.2022 - 19.06.2022',
          ),
          SizedBox(height: 50),
          BuildBodyContainer(
            title: 'Must Buy',
            image: 'assets/must-buy-img.png',
            name: 'Đầm Cotton Mini In Họa Tiết Ngắn Tay',
            description: 'váy mini kiểu cách với nhiều họa tiết xinh xắn',
            newPrice: '850.000 VND',
            oldPrice: '540.000 VND',
            discountText:
                'Khuyến Mãi Chỉ Áp Dụng Tại Các Hàng Trực Tuyến, SC Vivo City và Sài Gòn Centre',
            discountTime: 'Khuyến Mãi Có Hạn Từ 13.6.2022 - 19.06.2022',
          ),
          SizedBox(height: 50),
          BuildBodyContainer(
            title: 'Bộ sưu tập nổi bật',
            image: 'assets/bo-suu-tap-img.png',
            name: 'Quần Short Linen Cotton',
            description:
                'Chất liệu vải linen-cotton mát mẻ, sản phẩm tuyệt vời cho những ngày mùa hè',
            newPrice: '559.000 VND',
            oldPrice: '489.000 VND',
            discountText:
                'Khuyến Mãi Chỉ Áp Dụng Tại Các Hàng Trực Tuyến, SC Vivo City và Sài Gòn Centre',
            discountTime: 'Khuyến Mãi Có Hạn Từ 13.6.2022 - 19.06.2022',
          ),
          SizedBox(height: 50),
          Text(
            'Danh mục nổi bật'.toUpperCase(),
            style: TextStyle(fontSize: 24),
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: GridView.builder(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 120,
                mainAxisExtent: 180,
              ),
              padding: EdgeInsets.only(left: 10, right: 10),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: 8,
              itemBuilder: (context, index) => Container(
                margin: EdgeInsets.all(6),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 4),
                      child: CircleAvatar(
                        radius: 50,
                        backgroundImage: NetworkImage(
                            'https://lh3.googleusercontent.com/L-fJ68BmfgAR6F50fhRP313t3LcNO9aY16R15NYuaXNPg9wH6VmvzhANa6-1nFUChA_jGZ29TsM2vaMSE0qAhrkw2NVbuePFOWlYzNMzMQ=w824'),
                        child: SizedBox(
                          width: 50,
                          child: Text(
                            'Áo Sơ Mi/ Áo Kiểu',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 10, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 80,
                      child: Text(
                        'SHIRTS & BLOUSES'.toUpperCase(),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BuildBodyContainer extends StatelessWidget {
  final String title,
      image,
      name,
      description,
      newPrice,
      oldPrice,
      discountText,
      discountTime;

  const BuildBodyContainer({
    Key? key,
    required this.title,
    required this.image,
    required this.name,
    required this.description,
    required this.newPrice,
    required this.oldPrice,
    required this.discountText,
    required this.discountTime,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 16),
          child: Column(
            children: [
              Text(
                title.toUpperCase(),
                style: TextStyle(fontSize: 24),
              ),
              SizedBox(
                height: 24,
              ),
              Row(
                children: [
                  Image.asset(
                    image,
                    fit: BoxFit.fill,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            name,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              description,
                              style: TextStyle(
                                  fontSize: 13, color: Colors.grey[500]),
                            ),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  newPrice,
                                  style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Expanded(
                                child: Text(
                                  oldPrice,
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 13,
                                    decoration: TextDecoration.lineThrough,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 4),
                            child: Text(
                              discountText,
                              style: TextStyle(
                                color: Colors.red,
                                fontSize: 11,
                              ),
                            ),
                          ),
                          Text(
                            discountTime,
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 11,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(
                height: 40,
              ),
              GridView.builder(
                gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 180,
                  mainAxisExtent: 280,
                ),
                padding: EdgeInsets.only(left: 10, right: 10),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 9,
                itemBuilder: (context, index) => Container(
                  margin: EdgeInsets.all(6),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: Image.asset(
                          'assets/gridview-img.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 8),
                        height: 20,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: 4,
                          itemBuilder: (context, index) => Container(
                            width: 12,
                            margin: EdgeInsets.only(right: 6),
                            decoration: BoxDecoration(
                              color: Colors.lightBlueAccent,
                              border: Border.all(
                                width: 1,
                                color: Colors.grey,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 6, bottom: 6),
                        child: Text(
                          'Đầm Camisole Vải Satin',
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Text(
                        '799.00 VND',
                        style: TextStyle(
                          fontSize: 18,
                          decoration: TextDecoration.lineThrough,
                        ),
                      ),
                      Text(
                        '799.00 VND',
                        style: TextStyle(
                          fontSize: 21,
                          color: Colors.redAccent[400],
                        ),
                      ),
                      Expanded(
                        child: Text(
                          'Sale',
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.redAccent[400],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.black)),
                child: TextButton(
                  onPressed: () {},
                  child: Text(
                    'Xem tất cả'.toUpperCase(),
                    style: TextStyle(color: Colors.black),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
