import 'package:flutter/material.dart';
import 'cart_screen.dart';

class ScanScreen extends StatelessWidget {
  const ScanScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text('Quét'.toUpperCase()),
        actions: [
          GestureDetector(
            onTap: (){
              Navigator.of(context).pushNamed(CartScreen.route);
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 16),
              child: Icon(Icons.shopping_cart_outlined),
            ),
          ),
        ],
      ),
      backgroundColor: Colors.white,
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 32, bottom: 16),
              child: Image.asset('assets/scan-body-img.png'),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 16),
              child: Text(
                'Bạn có thể quét mã vạch của một sản phẩm để kiểm tra màu sắc, kích cỡ, và số lượng hàng sẵn có',
                textAlign: TextAlign.center,
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 32, vertical: 16),
              width: MediaQuery.of(context).size.width * 80,
              color: Colors.black,
              child: TextButton(
                onPressed: () {},
                child: Text(
                  'quét'.toUpperCase(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
