import 'package:flutter/material.dart';

class CouponsScreen extends StatelessWidget {
  static const route = '/coupons';

  const CouponsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 3,
        title: Text('Phiếu giảm giá'.toUpperCase()),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 40,
            ),
            ContainerWithBreakLine(
                leftText:
                    'Phiếu giảm giá cho các cửa hàng bán lẻ và cửa hàng trực tuyến',
                rightText: '1 (các) mã giảm giá',
                bottomText: ''),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[400]!,
                    blurRadius: 2,
                    spreadRadius: 1,
                  ),
                ],
              ),
              width: double.infinity,
              margin: EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: double.infinity,
                    child: Image.asset(
                      'assets/coupons-img.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: 8,
                          height: 8,
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        Text(
                          'Cho đến: 30/06/2022 11:59 CH',
                          style: TextStyle(fontSize: 13),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            ContainerWithBreakLine(
                leftText: 'Phiếu giảm giá cho cửa hàng bán lẻ',
                rightText: '0 (các) mã giảm giá',
                bottomText: 'Không có phiếu giảm giá'),
            SizedBox(
              height: 40,
            ),
            ContainerWithBreakLine(
                leftText: 'Phiếu giảm giá cho cửa hàng trực tuyến',
                rightText: '1 (các) mã giảm giá',
                bottomText: 'Không có phiếu giảm giá'),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }
}

class ContainerWithBreakLine extends StatelessWidget {
  final String leftText, rightText, bottomText;

  const ContainerWithBreakLine({
    Key? key,
    required this.leftText,
    required this.rightText,
    required this.bottomText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  child: Text(
                    leftText.toUpperCase(),
                    style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  width: 2,
                  height: 30,
                  color: Colors.grey,
                  margin: EdgeInsets.symmetric(horizontal: 8),
                ),
                Text(rightText),
              ],
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              bottomText,
              style: TextStyle(
                color: Colors.grey[400],
                fontSize: 13,
              ),
            )
          ],
        ));
  }
}
