import 'package:flutter/material.dart';

class SettingScreen extends StatelessWidget {
  static const route = '/setting';

  const SettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('cài đặt hồ sơ'.toUpperCase()),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      backgroundColor: Colors.grey[200],
      body: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    blurRadius: 1,
                    color: Colors.grey[400]!,
                    offset: Offset(0, 2),
                  ),
                ]
            ),
            child: Column(
              children: [
                _BuildListItem(
                  title: 'Chỉnh sửa hồ sơ',
                  breakLine: true,
                ),
                _BuildListItem(
                  title: 'Thay đổi mật khẩu của tôi',
                  breakLine: true,
                ),
                _BuildListItem(
                  title: 'Thẻ của tôi',
                  breakLine: true,
                ),
                _BuildListItem(
                  title: 'Số địa chỉ',
                  breakLine: false,
                ),
              ],
            )
          ),
          SizedBox(height: 16,),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  blurRadius: 1,
                  color: Colors.grey[400]!,
                  offset: Offset(0, 2),
                ),
              ]
            ),
            child: Column(
              children: [
                _BuildListItem(
                  title: 'đăng cuất',
                  breakLine: true,
                ),
                _BuildListItem(
                  title: 'Hủy tư cách thành viên trên ứng dụng',
                  breakLine: false,
                ),
              ],
            )
          ),
        ],
      ),
    );
  }
}

class _BuildListItem extends StatelessWidget {
  final String title;
  final bool breakLine;

  const _BuildListItem({
    Key? key,
    required this.title,
    required this.breakLine,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.white,
          child: TextButton(
            onPressed: () {},
            child: Container(
              padding: EdgeInsets.only(left: 8),
              alignment: Alignment.topLeft,
              child: Text(
                title.toUpperCase(),
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.normal,
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ),
        (breakLine == true)
            ? Container(
          margin: EdgeInsets.only(left: 16),
                height: 1,
                color: Colors.grey[400],
              )
            : Container(),
      ],
    );
  }
}
