import 'package:flutter/material.dart';

class AppSetting extends StatelessWidget {
  static const route = '/app-setting';

  const AppSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.blueGrey[800],
        foregroundColor: Colors.white,
        title: Text('Thông tin ứng dụng'),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.all(24),
                  child: Image.asset(
                    'assets/app-setting-icon.png',
                    fit: BoxFit.cover,
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'uniqlo'.toUpperCase(),
                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                    ),
                    SizedBox(height: 2),
                    Text(
                      'phiên bản 7.2.33',
                      style: TextStyle(color: Colors.grey[600]),
                    ),
                  ],
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                UninstallButton(title: 'Gỡ cài đặt'),
                UninstallButton(title: 'Buộc dừng'),
              ],
            ),
            _BuildListItem(
              title: 'Bộ nhớ',
              subTitle: '14,68 MB đã sử dụng trong Bộ nhớ trong',
              subTitleOn: true,
              enableButton: true,
            ),
            _BuildListItem(
              title: 'Quyền',
              subTitle: 'Chưa được cấp quyền nào',
              subTitleOn: true,
              enableButton: true,
            ),
            _BuildListItem(
              title: 'Thông báo',
              subTitle: '',
              subTitleOn: false,
              enableButton: true,
            ),
            _BuildListItem(
              title: 'Mở theo mặc định',
              subTitle: 'Chưa đặt theo mặc định',
              subTitleOn: true,
              enableButton: true,
            ),
            _BuildListItem(
              title: 'Pin',
              subTitle: 'Không sử dụng pin kể từ lần sạc đầy cuối cùng',
              subTitleOn: true,
              enableButton: false,
            ),
            _BuildListItem(
              title: 'Bộ nhớ',
              subTitle: '632 Mb bộ nhớ trung bình được sử dụng trong 3 giờ qua',
              subTitleOn: true,
              enableButton: true,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              child: Text('Của hàng', style: TextStyle(color: Colors.greenAccent[700], fontWeight: FontWeight.w500),),
            ),
            _BuildListItem(
              title: 'Chi tiết ứng dụng',
              subTitle: 'Ứng dụng đã được cài đặt từ Cửa hàng Google Play',
              subTitleOn: true,
              enableButton: true,
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}

class UninstallButton extends StatelessWidget {
  final String title;

  const UninstallButton({Key? key, required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      height: 40,
      width: 180,
      decoration: BoxDecoration(
          color: Colors.grey[300], borderRadius: BorderRadius.circular(3)),
      child: TextButton(
        onPressed: () {},
        child: Text(
          title.toUpperCase(),
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }
}

class _BuildListItem extends StatelessWidget {
  final String title, subTitle;
  final bool subTitleOn;
  final bool enableButton;

  const _BuildListItem({
    Key? key,
    required this.title,
    required this.subTitle,
    required this.subTitleOn,
    required this.enableButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          bottom: BorderSide(width: 1, color: Colors.grey[300]!),
        ),
      ),
      child: TextButton(
        onPressed: () {},
        child: Container(
          alignment: Alignment.topLeft,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 2),
                child: Text(
                  title,
                  style: TextStyle(
                    color: (enableButton)
                        ? Colors.black
                        : Colors.grey[350],
                    fontSize: 16,
                  ),
                ),
              ),
              (subTitleOn)
                  ? Text(
                      subTitle,
                      style: TextStyle(
                        color: (enableButton)
                            ? Colors.grey[500]
                            : Colors.grey[350],
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
