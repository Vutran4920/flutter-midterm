import 'package:flutter/material.dart';

class PurchaseHistory extends StatelessWidget {
  static const route = '/purchase-history';

  const PurchaseHistory({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Lịch sử mua hàng'.toUpperCase()),
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        elevation: 3,
      ),
      backgroundColor: Colors.grey[100],
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 22),
            child: Row(
              children: [
                Expanded(
                    child: Text(
                  'Lich sử mua hàng'.toUpperCase(),
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                )),
                Image.asset(
                  'assets/lich-su-mua-hang-icon.png',
                  fit: BoxFit.cover,
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(16),
            decoration: BoxDecoration(color: Colors.white, boxShadow: [
              BoxShadow(
                color: Colors.grey[400]!,
                blurRadius: 1,
                offset: Offset(0, 2),
              )
            ]),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Chúng tôi không có đơn hàng nào cho tài khoản này.'
                      .toUpperCase(),
                  style: TextStyle(
                    fontSize: 14.5,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                    'Nếu bạn đưa mã vạch thành viên của mình tại cửa hàng, các sản phẩm bạn mua sẽ được thêm vào lịch sử tài khoản của bạn.',
                    style: TextStyle(fontSize: 15))
              ],
            ),
          ),
          SizedBox(height: 20),
          Container(
            margin: EdgeInsets.all(16),
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(width: 1.5, color: Colors.black)),
            child: TextButton(
              onPressed: () {},
              child: Text(
                'Quay lại thành viên'.toUpperCase(),
                style: TextStyle(color: Colors.black, fontSize: 17),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
